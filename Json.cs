﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace JsonGeneric
{
    public static class Json
    {        
        public static string Serialize<T>(T obj, Formatting formatting)
            => JsonConvert.SerializeObject(obj, formatting);

        public static async Task<bool> WriteAsync<T>(IList<T> objList, string filename)
        {
            bool success = default(bool);
            try
            {
                foreach (var item in objList)
                    success = await WriteAsync<T>(item, filename);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }
            return success;
        }

        public static bool Write<T>(IList<T> objList, string filename)
        {
            bool success = default(bool);
            try
            {
                foreach (var item in objList)
                    success = Write<T>(item, filename);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }
            return success;
        }

        public static Task<bool> WriteAsync<T>(T obj, string filename) => Task.Run(() => Write<T>(obj, filename));
        public static bool Write<T>(T obj, string filename)
        {
            bool success = default(bool);
            try
            {
                string json = Serialize(obj, Formatting.Indented);

                using (var sw = new StreamWriter(filename))
                    sw.Write(json);

                success = true;
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(ex.Message);
                success = false;
            }

            return success;
        }
        public static T Deserialize<T>(string json)
            => JsonConvert.DeserializeObject<T>(json);
        public static IList<T> DeserializeAll<T>(string json)
            => JsonConvert.DeserializeObject<IList<T>>(json);

        public static IDictionary<K, V> DeserializeDictionaryAll<K, V>(string json)
            => JsonConvert.DeserializeObject<IDictionary<K, V>>(json);

        public static Task<IList<T>> ReadAllAsync<T>(string filename) => Task.Run(() => ReadAll<T>(filename));
        public static IList<T> ReadAll<T>(string filename)
        {
            IList<T> objs = new List<T>();
            try
            {
                var json = TextReader.Read(filename);
                objs = DeserializeAll<T>(json);                
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }
            return objs;
        }                     

        public static Task<IDictionary<K, V>> ReadDictionaryAllAsync<K, V>(string filename) => Task.Run(() => ReadDictionaryAll<K, V>(filename));
        public static IDictionary<K, V> ReadDictionaryAll<K, V>(string filename)
        {
            IDictionary<K, V> objs = new Dictionary<K, V>();
            try
            {
                var json = TextReader.Read(filename);
                objs = DeserializeDictionaryAll<K, V>(json);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }
            return objs;
        }

        public static Task<T> ReadAsync<T>(string filename) => Task.Run(() => Read<T>(filename));
        public static T Read<T>(string path)
        {
            T obj = default(T);
            try
            {
                var json = TextReader.Read(path);
                obj = Deserialize<T>(json);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }
            return obj;
        }
    }
}
