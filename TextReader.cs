﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace JsonGeneric
{
    public static class TextReader
    {
        public static Task<IList<string>> ReadLinesAsync<T>(string filename) => Task.Run(() => ReadLines(filename));
        public static IList<string> ReadLines(string filename)
        {
            var text = new List<string>();
            try
            {
                using (var sm = new StreamReader(filename))
                {
                    var temp = sm.ReadLine();
                    while (temp != null)
                    {
                        text.Add(temp);
                        temp = sm.ReadLine();
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }
            return text;
        }

        public static Task<string> ReadAsync(string filename) => Task.Run(() => Read(filename));
        public static string Read(string filename)
        {
            string text = default(string);
            try
            {
                using (var sr = new StreamReader(filename))
                {
                    text = sr.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }
            return text;
        }
    }
}
